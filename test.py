import unittest
import functions



class TestSum(unittest.TestCase):

    def test_sum(self):
        self.assertEqual(functions.add(x,y), 4, "Should be 4")

    def test_sumnoteq(self):
        self.assertNotEqual(functions.add(x,y), 6, "Should not be equal to 6")

    def test_str(self):
        self.assertEqual(functions.Concatenate(a,b), "yesno", "Should be yesno.")

    def test_strnoteq(self):
        self.assertNotEqual(functions.Concatenate(a,b), "noyes", "Should not be noyes.")




a = "yes"
b = "no"

x = float(1.5)
y = float(2.5)

test = TestSum()

if __name__ == '__main__':
    test.test_sum()
    test.test_sumnoteq()
    test.test_str()
    test.test_strnoteq()
    print("All tests passed.")
